<?php

class MysqlChangePasswordDriver implements \RainLoop\Providers\ChangePassword\ChangePasswordInterface
{
	/**
	 * @var string
	 */
	private $sAllowedEmails = '';

	/**
	 * @param string $sAllowedEmails
	 *
	 * @return \ChangePasswordExampleDriver
	 */
	public function SetAllowedEmails($sAllowedEmails)
	{
		$this->sAllowedEmails = $sAllowedEmails;
		return $this;
	}

	/**
	 * @param \RainLoop\Model\Account $oAccount
	 *
	 * @return bool
	 */
	public function PasswordChangePossibility($oAccount)
	{
		return $oAccount && $oAccount->Email() &&
			\RainLoop\Plugins\Helper::ValidateWildcardValues($oAccount->Email(), $this->sAllowedEmails);
	}

	/**
	 * @param \RainLoop\Model\Account $oAccount
	 * @param string $sPrevPassword
	 * @param string $sNewPassword
	 *
	 * @return bool
	 */
	public function ChangePassword(\RainLoop\Account $oAccount, $sPrevPassword, $sNewPassword)
	{
		$bResult = false;

		$config = array (
			'host' => '127.0.0.1',
			'user' => 'vmail',
			'pass' => '',
			'db'   => 'vmail',
			);

		$mailparts = explode('@', $oAccount->Email());
		$user = $mailparts[0];
		$domain = $mailparts[1];

		$salt = substr(sha1(rand()), 0, 16);
		$sHashedNewPassword = "{SHA512-CRYPT}" . crypt($sNewPassword, "$6$$salt");

    		$mysqli = new mysqli ($config['host'], $config['user'], $config['pass'], $config['db']);
		$result = $mysqli->query ("UPDATE accounts SET password='". $sHashedNewPassword ."' WHERE `username`='". $mailparts[0] ."' AND `domain`='". $mailparts[1] ."'");

		if ($result)
			$bResult = true;
		return $bResult;
	}
}
